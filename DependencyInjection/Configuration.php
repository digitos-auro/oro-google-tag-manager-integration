<?php

namespace Ibp\Bundle\IbpBundle\DependencyInjection;

use Oro\Bundle\ConfigBundle\DependencyInjection\SettingsBuilder;
use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root(GoogleTagManagerExtension::ALIAS);

        SettingsBuilder::append(
            $rootNode,
            [
                'google_tag_manager_container_id' => ['type' => 'string', 'value' => ''],
            ]
        );

        return $treeBuilder;
    }
}
