<?php

namespace DigitosAuro\Bundle\GoogleTagManagerBundle\Layout\DataProvider;

use Oro\Bundle\ConfigBundle\Config\ConfigManager;

class SettingsProvider
{
    /**
     * @var ConfigManager
     */
    private $configManager;

    /**
     * @param ConfigManager $configManager
     */
    public function __construct(ConfigManager $configManager)
    {
        $this->configManager = $configManager;
    }

    /**
     * @return string
     */
    public function getGoogleTagManagerContainerId()
    {
        $widgetCode = trim($this->configManager->get('digitosauro.google_tag_manager_container_id'));

        return $widgetCode ? : null;
    }
}
